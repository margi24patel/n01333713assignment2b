﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WebApplication1.usercontrols
{
    public partial class CodeBox : System.Web.UI.UserControl
    {

       public string code
        {
            get { return (string)ViewState["code"]; }
            set { ViewState["code"] = value; }
        }

        public string owner
        {
            get { return (string)ViewState["owner"]; }
            set { ViewState["owner"] = value; }
        }



        DataView CreateSourceCode(List<string> listOfCode)
        {
            DataTable jscode = new DataTable();

            DataColumn idx_col = new DataColumn();

            DataColumn code_col = new DataColumn();

           

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            jscode.Columns.Add(idx_col);


            
            code_col.ColumnName = " Code";
            code_col.DataType = System.Type.GetType("System.String");
            jscode.Columns.Add(code_col);


            int i = 0;
            foreach (string code_line in listOfCode)
            {
                coderow = jscode.NewRow(); 
               

                coderow[idx_col.ColumnName] = i;
                string final_result_code = System.Net.WebUtility.HtmlEncode(code_line);
                final_result_code = final_result_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = final_result_code;

                i++;
                jscode.Rows.Add(coderow); 
               

            }

            DataView codeview = new DataView(jscode); 
            return codeview;

            
            
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> teacher_code_of_js = new List<string>(new string[]
            {
               "~var book = [\"Ramayana\", \"3Idiots\", \"Wings On Fire\"];"
            });

            List<string> my_code_of_js = new List<string>(new string[]
            {

                "~var userChoice;",
                "~var yesMessage = \"your email has been added to our list;\"",
                "~var noMessage = \"we will not bother you again;\"",
                "~var messageOut;",

                "~userChoice = \"confirm(would like to sign up for your email newsletter?);\"",

               "~if (userChoice == true)",
                "~~{",
                  "~~~messageOut = yesMessage;",
                "~~}",

               "~else",
                "~~{",
                 "~~~messageOut = noMessage;",
                "~~}",
                "~alert(\"Thank you! \" + messageOut);"
            });




            List<string> teacher_code_of_html = new List<string>(new string[]
           {
               "~<ol type=\"1\">",
                "~~<li>Coffee</li>",
                 "~~<li>Tea</li>",
                "~~<li>Milk</li>",
               "~ </ol>"

        });



            List<string> my_code_of_html = new List<string>(new string[]
          {

                "~<div>",
                 "~~<label> No.Of Person: <label>",
                 "~~~<input type = \"radio\" id = \"couple\" name=\"no.ofPerson\" value = \"couple\" >",
                 "~~<label for = \"couple\"> Couple </label>",
                 "~~~<input type = \"radio\" id = \"family\" name=\"no.ofPerson\" value = \"family\" >",
                 "~~<label for = \"family\"> Family </label>",
                 "~<div>"

             });

            List<string> teacher_code_of_sql = new List<string>(new string[]
           {
               " ~SELECT *",
               " ~FROM invoices i",
               " ~~FULL JOIN vendors v",
                "~~~ON i.vendor_id = v.vendor_id",
                "~~FULL JOIN vendor_contacts vc",
                "~~~ON v.vendor_id = vc.vendor_id"

            });

            List<string> my_code_of_sql = new List<string>(new string[]
           {
                 "--all the invoices that don't have a listing in invoice_line_items--",
                "SELECT *",

                "FROM invoices i",

                "~LEFT JOIN invoice_line_items li",
                "~~ON i.invoice_id = li.invoice_id",
                "~WHERE i.invoice_id IS NULL"

             });

            List<string> final_result_code = new List<string>(new String[] { });
            if(code == "JS" && owner == "Me")
            {
                final_result_code = my_code_of_js;
            }


            if (code == "JS" && owner == "Teacher")
            {
                final_result_code = teacher_code_of_js;
            }

            if (code == "HTML" && owner == "Me")
            {
                final_result_code = my_code_of_html;
            }

            if (code == "HTML" && owner == "Teacher")
            {
                final_result_code = teacher_code_of_html;
            }

            if (code == "SQL" && owner == "Me")
            {
                final_result_code = my_code_of_sql;
            }

            if (code == "SQL" && owner == "Teacher")
            {
                final_result_code = teacher_code_of_sql;
            }


            
            DataView ds = CreateSourceCode(final_result_code);
            code_Container.DataSource = ds;

            code_Container.DataBind();



        }
    }
}