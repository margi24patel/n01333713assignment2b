﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQL.aspx.cs" Inherits="WebApplication1.WebForm2" %>


<asp:Content ID="headingSQL" ContentPlaceHolderID="Heading" runat="server">
    <h1 style="text-align:center">SQL DataBase Design</h1>
</asp:Content>

<asp:Content ID="ilearnedSQL" ContentPlaceHolderID="MainContent" runat="server">

    <h3>SQL Join Example </h3>
    <p>
        1. Write a query that joins invoices, vendors, and vendor_contacts that won't exclude any
        rows from any of the tables
    </p>
    <%--<h4>Query</h4>--%>
    <p>
        <%--SELECT *<br />
        FROM invoices i<br />
        FULL JOIN vendors v<br />
        ON i.vendor_id = v.vendor_id<br />
        FULL JOIN vendor_contacts vc<br />
        ON v.vendor_id = vc.vendor_id--%>

    </p>
    
     <%-- i added usercontrol for teacher--%>
    <uctrl:usercode ID="Teacher_Side_SQL_Code" runat="server" SkinId="GridViewSkin" code="SQL" 
        owner="Teacher"></uctrl:usercode>


    <h4>Description</h4>
    <p>
        Here, are three tables named as invoices,vendors and vendor_contacts.<br />
        Whereas, vendors Full join to invoices through vendor_id. Moreover,<br />
        invoices and vendors Full join to vendor_contacts through vendor_id.<br />
        It will display all the records from all three tables(invoices,vendors and vendor_contacts).
    </p>


    
</asp:Content>


<asp:Content ID="iwroteSQL" ContentPlaceHolderID="MyCode" runat="server">
    <h3>SQL Left Join Example</h3>
    <p>2.Write a query that shows all the invoices that don't have a listing in invoice_line_items.</p>
   <%-- <h4>Query</h4>
    <p>
        
    </p>--%>

    <%--DataGrid for SQL--%>
   <%-- <asp:DataGrid ID="Code_sql" runat="server" CssClass="code" GridLines="None" Width="500px"
        CellPadding="2">

        <HeaderStyle Font-Size="Large" BackColor="#ff9900" ForeColor="#ffffff" />
        <ItemStyle BackColor="#333300" ForeColor="#ffffff" />
   
    </asp:DataGrid>--%>

<%--    i added usercontrol for me --%>
     <uctrl:usercode ID="My_Side_SQL_Code" runat="server" SkinId="GridViewSkin" code="SQL" 
        owner="Me"></uctrl:usercode>


    <h4>Description</h4>
    <p>
        Here, are two tables of invoices and invoice_line_items.<br />
        We are joining two tables by using Left Joins through invoice_id.<br />

        
    </p>

   
</asp:Content>
<asp:Content ID="ifoundSQL" ContentPlaceHolderID="ExampleCode" runat="server">

    <h3>SQL Joins</h3>
    <h4>Diiferent Types Of Join:</h4>
    <p>Here are the different types of the JOINs in SQL:</p>

    <p>
        (INNER) JOIN: Returns records that have matching values in both tables<br />
        LEFT (OUTER) JOIN: Return all records from the left table, and the matched records from the right table<br />
        RIGHT (OUTER) JOIN: Return all records from the right table, and the matched records from the left table<br />
        FULL (OUTER) JOIN: Return all records when there is a match in either left or right table.
    </p>


   <%-- <h4>SQL Join Example</h4>
    <p>
        3.Write a query that lists all the vendor names, and if they have invoices, list the line
        item description.
    </p>
    <h4>Query</h4>
    <p>
        SELECT v.vendor_id, vendor_name,i.invoice_id, line_item_description<br />
        FROM vendors v<br />
        JOIN invoices i<br />
        ON v.vendor_id = i.vendor_id<br />
        JOIN invoice_line_items li<br />
        ON i.invoice_id = li.invoice_id
    </p>
--%>

</asp:Content>
<asp:Content ID="listoflinkSQL" ContentPlaceHolderID="Link" runat="server">

    <h3>Some Helpful Links:</h3>
    <ul>
        <li><a href="https://www.w3schools.com/sql/sql_join.asp">www.W3Schools.com</a></li>
        <li><a href="https://www.lynda.com/Web-training-tutorials/88-0.html">www.Lynda.com</a></li>
    </ul>
</asp:Content>
