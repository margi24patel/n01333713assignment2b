﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HTML.aspx.cs" Inherits="WebApplication1.WebForm3" %>

<asp:Content ID="headingHTML" ContentPlaceHolderID="Heading" runat="server">
    <h1 style="text-align:center">HTML </h1>
</asp:Content>

<asp:Content ID="ilearnedHTML" ContentPlaceHolderID="MainContent" runat="server">
    
    

    <h3>HTML Ordered List</h3>
    <%--<h4>Code:</h4>--%>

   <%-- &lt;ol type=&quot;1&quot;&gt;<br />
    &lt;Tea&gt;<br />
    &lt;Milk&gt;<br />
    &lt;/ol&gt;--%>

    <%--    i added usercontrol for techer --%>
     <uctrl:usercode ID="Teacher_Side_HTML_Code" runat="server" SkinId="GridViewSkin" code="HTML" 
        owner="Teacher"></uctrl:usercode>

    <h4>Description:</h4>
    <p>
        An ordered list starts with the &lt;ol&gt; tag. Each list item starts with the &lt;li&gt; tag.<br />

        The list items will be marked with numbers by default: &lt;li&gt; tag.
    </p>

   

 
</asp:Content>

<asp:Content ID="iwroteHTML" ContentPlaceHolderID="MyCode" runat="server">
    <h3>Radio Button</h3>
   <%-- <h4>Code:</h4>

    <p>
        
    </p>--%>

   <%-- DataGrid for HTML--%>
    <%--<asp:DataGrid ID="Code_Html" runat="server" CssClass="code" GridLines="None" Width="530px"
        CellPadding="2">

        <HeaderStyle Font-Size="Large" BackColor="#ff9900" ForeColor="#ffffff" />
        <ItemStyle BackColor="#333300" ForeColor="#ffffff" />
   
    </asp:DataGrid>--%>


    <%--    i added usercontrol for me --%>

    <uctrl:usercode ID="My_Side_HTML_Code" runat="server" SkinId="GridViewSkin" code="HTML" 
        owner="Me"></uctrl:usercode>


    <h4>Description:</h4>
    <p>
        Radio Buttons are used when the user must make only one selection out of a group of items.
        <br />
        Add radio buttons to a group by adding the name attribute along with the same corresponding value for each of the radio buttons in the group.
        Here,user can select only one item.ForEx,to select a couple OR family 
    </p>

</asp:Content>

<asp:Content ID="ifoundHTML" ContentPlaceHolderID="ExampleCode" runat="server">
    


    <h3>HTML Layouts</h3>
    <h5>Different parts of WebPage</h5>
    <img src="images/Layout%20Image%20ASP.NET.gif" />
    <div>
        <ul>
            <li>header - header for a document or a section.</li>
            <li>nav - container for navigation links.</li>
            <li>section - section in a document.</li>
            <li>article - independent self-contained article.</li>
            <li>aside - content aside from the content(sidebar).</li>
            <li>footer - footer for a WebPage</li>
            <li>details - additional details</li>

        </ul>
    </div>
    <p>This is a Trickey Part for me to adjust webpage parts.</p>
</asp:Content>

<asp:Content ID="listoflinkHTML" ContentPlaceHolderID="Link" runat="server">
    <h3>Some Helpful Links:</h3>
    <ul>
        <li><a href="https://www.w3schools.com/html/html_styles.asp">www.W3Schools.com</a></li>
        <li><a href="https://www.pluralsight.com/">www.pluralsight.com</a></li>
    </ul>
</asp:Content>
